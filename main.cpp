#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio/windows/random_access_handle.hpp>
#include <iostream>
#include <vector>
#include <windows.h>
#include "random_access_handle_extended.hpp"

using namespace boost::asio;
using namespace boost::system;

io_context context;

void read_complete(const boost::system::error_code& ec, std::size_t bytes_transferred) {
    if (!ec) {
        std::cout << "Read " << bytes_transferred << " bytes successfully." << std::endl;
    } else {
        std::cerr << "Read error: " << ec.message() << std::endl;
    }
}

void write_complete(const boost::system::error_code& ec, std::size_t bytes_transferred, random_access_handle_extended& file, char* read_buffer, std::size_t read_buffer_size) {
    if (!ec) {
        std::cout << "Write " << bytes_transferred << " bytes successfully." << std::endl;

        // Now read the data back
        file.async_read_some( boost::asio::buffer(read_buffer, read_buffer_size), [read_buffer](const boost::system::error_code& ec, std::size_t bytes_transferred) {
            read_complete(ec, bytes_transferred);
            delete[] read_buffer; // Clean up
        });
    } else {
        std::cerr << "Write error: " << ec.message() << std::endl;
        delete[] read_buffer; // Clean up on error as well
    }
}

int main() {
    // Create or open the file
    random_access_handle_extended file(context.get_executor());
    HANDLE file_handle = CreateFile("example.txt", GENERIC_WRITE | GENERIC_READ, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    if (file_handle == INVALID_HANDLE_VALUE) {
        std::cerr << "Failed to open file." << std::endl;
        return 1;
    }

    file.assign(file_handle);

    // Prepare the data to write
    std::string write_data = "Hello, Boost.Asio with Random Access Handle!";
    auto* read_buffer = new char[write_data.size()];

    // Asynchronously write data to the file
    file.async_write_some(boost::asio::buffer(write_data), [&file, read_buffer, write_data](const boost::system::error_code& ec, std::size_t bytes_transferred) {
        write_complete(ec, bytes_transferred, file, read_buffer, write_data.size());
    });

    context.run();

    CloseHandle(file_handle);
    return 0;
}
